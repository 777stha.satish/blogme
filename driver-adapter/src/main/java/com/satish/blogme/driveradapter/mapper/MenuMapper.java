package com.satish.blogme.driveradapter.mapper;

import com.satish.blogme.domain.model.MenuModel;
import com.satish.blogme.driveradapter.dto.MenuDTO;

public interface MenuMapper extends SingleGenericMapper<MenuDTO, MenuModel> {
}
