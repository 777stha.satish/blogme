package com.satish.blogme.driveradapter.mapper.collection;

import java.util.Collection;

public interface CollectionMapper<D, M> {
    Collection<D> toDTOs(Collection<M> models);
    Collection<M> toModels(Collection<D> dtos);
}
