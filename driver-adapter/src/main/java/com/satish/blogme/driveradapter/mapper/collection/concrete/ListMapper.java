package com.satish.blogme.driveradapter.mapper.collection.concrete;

import com.satish.blogme.driveradapter.mapper.SingleGenericMapper;
import com.satish.blogme.driveradapter.mapper.collection.CollectionMapper;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

public class ListMapper<D, M> extends StreamMapper<D, M> implements CollectionMapper<D, M> {

    public ListMapper(SingleGenericMapper<D, M> singleMapper) {
        super(singleMapper);
    }

    @Override
    public Collection<D> toDTOs(Collection<M> models) {
        if (models == null || models.size() == 0) {
            return Collections.emptyList();
        }
        return mapToStreamDTO(models)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<M> toModels(Collection<D> dtos) {
        if (dtos == null || dtos.size() == 0) {
            return Collections.emptyList();
        }
        return mapToStreamModel(dtos)
                .collect(Collectors.toList());
    }
}
