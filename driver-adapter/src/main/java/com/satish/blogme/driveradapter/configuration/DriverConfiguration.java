package com.satish.blogme.driveradapter.configuration;

import com.satish.fieldvalidator.primaryadapter.configuration.FieldValidatorDriverConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@ComponentScan(basePackages = {"com.satish.blogme.driveradapter"})
@EnableWebMvc
@PropertySource("classpath:driver.properties")
public class DriverConfiguration extends FieldValidatorDriverConfiguration {
}
