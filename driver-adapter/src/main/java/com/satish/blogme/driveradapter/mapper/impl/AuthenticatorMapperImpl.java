package com.satish.blogme.driveradapter.mapper.impl;

import com.satish.blogme.domain.model.AuthenticatorModel;
import com.satish.blogme.driveradapter.mapper.AuthenticatorMapper;
import com.satish.blogme.driveradapter.dto.AuthenticatorDTO;
import org.springframework.stereotype.Service;

@Service
public class AuthenticatorMapperImpl implements AuthenticatorMapper {

    @Override
    public AuthenticatorDTO toDTO(AuthenticatorModel authenticatorModel) {
        if (null == authenticatorModel) {
            return null;
        }
        return new AuthenticatorDTO(authenticatorModel.getUsername(), authenticatorModel.getPassword());
    }

    @Override
    public AuthenticatorModel toModel(AuthenticatorDTO authenticatorDTO) {
        if (null == authenticatorDTO) {
            return null;
        }
        return new AuthenticatorModel(authenticatorDTO.getUsername(), authenticatorDTO.getPassword());
    }
}
