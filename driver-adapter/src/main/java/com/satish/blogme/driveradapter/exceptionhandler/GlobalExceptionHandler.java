package com.satish.blogme.driveradapter.exceptionhandler;

import com.satish.fieldvalidator.primaryadapter.dto.Error;
import com.satish.fieldvalidator.primaryadapter.exception.ValidatorGlobalExceptionHandler;
import com.satish.blogme.domain.exception.RestTemplateException;
import com.satish.blogme.driveradapter.dto.HttpErrorMessage;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;
import java.util.Set;

@RestControllerAdvice
public class GlobalExceptionHandler extends ValidatorGlobalExceptionHandler {
    public GlobalExceptionHandler(final Set<Error> errors) {
        super(errors);
    }

    @ExceptionHandler(RestTemplateException.class)
    public ResponseEntity<HttpErrorMessage> handleRestTemplateException(RestTemplateException ex) throws IOException {
        HttpErrorMessage httpErrorMessage =  new ObjectMapper().readValue(ex.getMessage(), HttpErrorMessage.class);
        return ResponseEntity.status(HttpStatus.valueOf(httpErrorMessage.getCode())).body(httpErrorMessage);
    }
}
