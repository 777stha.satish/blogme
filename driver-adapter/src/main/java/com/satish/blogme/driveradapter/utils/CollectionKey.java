package com.satish.blogme.driveradapter.utils;

public enum CollectionKey {
    SET,
    LIST
}
