package com.satish.blogme.driveradapter.utils;

public final class ApiPathConstant {

    private ApiPathConstant() {}

    public final static String BASE_PATH_V1 = "/api/v1";

    public final static String PUBLIC = "/public";

    public final static String LOGIN = "/login";

    public final static String MENU = "/menu";
}
