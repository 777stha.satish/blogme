package com.satish.blogme.driveradapter.controller;

import com.satish.blogme.domain.port.driver.AuthenticationUseCase;
import com.satish.blogme.driveradapter.mapper.AuthenticatorMapper;
import com.satish.blogme.driveradapter.dto.AuthenticatorDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.io.IOException;

import static com.satish.blogme.driveradapter.utils.ApiPathConstant.*;

@RestController
@RequestMapping(BASE_PATH_V1 + PUBLIC + LOGIN)
public class AuthenticationController {

    private final AuthenticationUseCase authenticationUseCase;

    private final AuthenticatorMapper authenticatorMapper;

    public AuthenticationController(final AuthenticationUseCase loginUseCase,
                                    final AuthenticatorMapper authenticatorMapper) {
        this.authenticationUseCase = loginUseCase;
        this.authenticatorMapper = authenticatorMapper;
    }

    @PostMapping
    public ResponseEntity<OAuth2AccessToken> login(@RequestBody @Valid AuthenticatorDTO authenticatorDTO) throws IOException {
        return ResponseEntity.ok(authenticationUseCase
                .retrieveAccessToken(authenticatorMapper.toModel(authenticatorDTO))
        );
    }
}
