package com.satish.blogme.driveradapter.mapper.collection.concrete;

import com.satish.blogme.driveradapter.mapper.SingleGenericMapper;

import java.util.Collection;
import java.util.stream.Stream;

public abstract class StreamMapper<D, M> {

    private SingleGenericMapper<D, M> singleMapper;

    public StreamMapper(SingleGenericMapper<D, M> singleMapper) {
        this.singleMapper = singleMapper;
    }

    public Stream<M> mapToStreamModel(Collection<D> dtos) {
        return dtos.stream()
                .map(singleMapper::toModel);
    }

    public Stream<D> mapToStreamDTO(Collection<M> models) {
        return models.stream()
                .map(singleMapper::toDTO);
    }
}
