package com.satish.blogme.driveradapter;

import com.satish.blogme.driveradapter.configuration.DriverConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackageClasses = {DriverConfiguration.class})
public class DriverAdapaterApplication {

	public static void main(String[] args) {
		SpringApplication.run(DriverAdapaterApplication.class, args);
	}

}
