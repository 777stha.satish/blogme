package com.satish.blogme.driveradapter.controller;

import com.satish.blogme.domain.model.MenuModel;
import com.satish.blogme.domain.port.driver.MenuUseCase;
import com.satish.blogme.driveradapter.mapper.collection.CollectionMapper;
import com.satish.blogme.driveradapter.mapper.collection.concrete.CollectionMapperFactory;
import com.satish.blogme.driveradapter.dto.MenuDTO;
import com.satish.blogme.driveradapter.mapper.SingleGenericMapper;
import com.satish.blogme.driveradapter.utils.CollectionKey;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.satish.blogme.driveradapter.utils.ApiPathConstant.*;

@RestController
@RequestMapping(BASE_PATH_V1 + MENU)
public class MenuController {

    private final MenuUseCase menuUseCase;

    private final CollectionMapper<MenuDTO, MenuModel> listMapper;

    public MenuController(final MenuUseCase menuUseCase, final SingleGenericMapper<MenuDTO, MenuModel> singleGenericMapper) {
        this.menuUseCase = menuUseCase;
        this.listMapper = CollectionMapperFactory.getMapper(CollectionKey.LIST, singleGenericMapper);
    }

    @GetMapping
    public ResponseEntity<List<MenuDTO>> getMenusByEnabled(@RequestParam boolean enabled) {
      List<MenuDTO> menuDTOS = (List<MenuDTO>) listMapper.toDTOs(menuUseCase.getMenuByEnabled(enabled));
      return ResponseEntity.ok(menuDTOS);
    }

}
