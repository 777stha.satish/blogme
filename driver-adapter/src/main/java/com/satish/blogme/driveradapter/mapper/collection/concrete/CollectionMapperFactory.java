package com.satish.blogme.driveradapter.mapper.collection.concrete;

import com.satish.blogme.driveradapter.mapper.SingleGenericMapper;
import com.satish.blogme.driveradapter.mapper.collection.CollectionMapper;
import com.satish.blogme.driveradapter.utils.CollectionKey;

public class CollectionMapperFactory {

    private CollectionMapperFactory(){}

    public static <D, M> CollectionMapper<D, M> getMapper(CollectionKey key, SingleGenericMapper<D, M> singleMapper) {
        if (key == null) {
            return null;
        }
        switch (key) {
            case SET:
                return new SetMapper<>(singleMapper);
            case LIST:
                return new ListMapper<>(singleMapper);
            default:
                return null;

        }
    }
}
