package com.satish.blogme.driveradapter.mapper;

import com.satish.blogme.domain.model.AuthenticatorModel;
import com.satish.blogme.driveradapter.dto.AuthenticatorDTO;

public interface AuthenticatorMapper extends SingleGenericMapper<AuthenticatorDTO, AuthenticatorModel> {
}
