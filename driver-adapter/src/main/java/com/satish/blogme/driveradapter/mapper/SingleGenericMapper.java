package com.satish.blogme.driveradapter.mapper;

public interface SingleGenericMapper<D, M> {

    D toDTO(M model);

    M toModel(D dto);
}
