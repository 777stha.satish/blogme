package com.satish.blogme.driveradapter.mapper.impl;

import com.satish.blogme.domain.model.MenuModel;
import com.satish.blogme.driveradapter.mapper.MenuMapper;
import com.satish.blogme.driveradapter.dto.MenuDTO;
import org.springframework.stereotype.Service;

@Service
public class MenuMapperImpl implements MenuMapper {

    @Override
    public MenuModel toModel(MenuDTO menuDTO) {

        if (menuDTO == null) {
            return null;
        }

        MenuModel menuModel = new MenuModel();
        menuModel.setName(menuDTO.getName());
        menuModel.setPath(menuDTO.getPath());
        menuModel.setEnabled(menuDTO.isEnabled());
        menuModel.setOrder(menuDTO.getOrder());
        menuModel.setIcon(menuDTO.getIcon());

        return menuModel;
    }

    @Override
    public MenuDTO toDTO(MenuModel model) {

        if (model == null) {
            return null;
        }

        MenuDTO menu = new MenuDTO();
        menu.setName(model.getName());
        menu.setPath(model.getPath());
        menu.setEnabled(model.isEnabled());
        menu.setOrder(model.getOrder());
        menu.setIcon(model.getIcon());

        return menu;
    }
}
