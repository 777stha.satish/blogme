package com.satish.blogme.driveradapter.mapper.collection.concrete;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import com.satish.blogme.driveradapter.mapper.SingleGenericMapper;
import com.satish.blogme.driveradapter.mapper.collection.CollectionMapper;


public class SetMapper<D, M> extends StreamMapper<D, M> implements CollectionMapper<D, M> {

    public SetMapper(SingleGenericMapper<D, M> singleMapper) {
        super(singleMapper);
    }

    @Override
    public Collection<D> toDTOs(Collection<M> models) {
        if (models == null || models.size() == 0) {
            return Collections.emptySet();
        }
        return mapToStreamDTO(models)
                .collect(Collectors.toSet());
    }

    @Override
    public Collection<M> toModels(Collection<D> dtos) {
        if (dtos == null || dtos.size() == 0) {
            return Collections.emptySet();
        }
        return mapToStreamModel(dtos)
                .collect(Collectors.toSet());
    }
}
