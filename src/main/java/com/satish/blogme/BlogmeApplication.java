package com.satish.blogme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogmeApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogmeApplication.class, args);
	}

}
