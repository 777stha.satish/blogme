package com.satish.blogme.domain.port.driven;

import com.satish.blogme.domain.model.AuthenticatorModel;
import com.satish.blogme.domain.port.AuthenticationPort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.net.URI;

public interface AuthenticationRepoPort extends AuthenticationPort {

    HttpHeaders getHeader();

    URI getURI(AuthenticatorModel authenticatorModel);

    ResponseEntity<String> requestAuthServer(HttpHeaders headers, URI uri);
}
