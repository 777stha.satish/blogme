package com.satish.blogme.domain.model.base;

public class Identity<I> {
    private I id;

    public I getId() {
        return id;
    }

    public void setId(I id) {
        this.id = id;
    }
}
