package com.satish.blogme.domain.port.driver;

import com.satish.blogme.domain.port.MenuPort;

public interface MenuUseCase extends MenuPort {
}
