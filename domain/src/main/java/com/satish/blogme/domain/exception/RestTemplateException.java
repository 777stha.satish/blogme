package com.satish.blogme.domain.exception;

public class RestTemplateException extends RuntimeException {
    public RestTemplateException(String message) {
        super(message);
    }
}
