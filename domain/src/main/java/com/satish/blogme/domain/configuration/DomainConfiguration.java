package com.satish.blogme.domain.configuration;

import com.satish.fieldvalidator.hexagon.configuration.FieldValidatorDomConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.satish.blogme.domain")
public class DomainConfiguration extends FieldValidatorDomConfiguration {
}
