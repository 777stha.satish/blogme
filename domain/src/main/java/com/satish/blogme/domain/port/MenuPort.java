package com.satish.blogme.domain.port;

import com.satish.blogme.domain.model.MenuModel;

import java.util.List;

public interface MenuPort {
    List<MenuModel> getMenuByEnabled(boolean enabled);
}
