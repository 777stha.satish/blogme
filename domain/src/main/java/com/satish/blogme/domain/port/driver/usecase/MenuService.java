package com.satish.blogme.domain.port.driver.usecase;

import com.satish.blogme.domain.model.MenuModel;
import com.satish.blogme.domain.port.driven.MenuRepoPort;
import com.satish.blogme.domain.port.driver.MenuUseCase;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuService implements MenuUseCase {

    private final MenuRepoPort menuRepoPort;

    public MenuService(final MenuRepoPort menuRepoPort) {
        this.menuRepoPort = menuRepoPort;
    }

    @Override
    public List<MenuModel> getMenuByEnabled(boolean enabled) {
        return menuRepoPort.getMenuByEnabled(enabled);
    }
}
