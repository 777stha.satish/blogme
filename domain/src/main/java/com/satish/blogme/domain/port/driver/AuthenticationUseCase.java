package com.satish.blogme.domain.port.driver;

import com.satish.blogme.domain.port.AuthenticationPort;

public interface AuthenticationUseCase extends AuthenticationPort {
}
