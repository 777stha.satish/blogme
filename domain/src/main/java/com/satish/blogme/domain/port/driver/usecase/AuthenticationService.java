package com.satish.blogme.domain.port.driver.usecase;

import com.satish.blogme.domain.model.AuthenticatorModel;
import com.satish.blogme.domain.port.driven.AuthenticationRepoPort;
import com.satish.blogme.domain.port.driver.AuthenticationUseCase;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class AuthenticationService implements AuthenticationUseCase {

    private final AuthenticationRepoPort authenticationRepoPort;

    public AuthenticationService(final AuthenticationRepoPort loginRepoPort) {
        this.authenticationRepoPort = loginRepoPort;
    }

    @Override
    public OAuth2AccessToken retrieveAccessToken(AuthenticatorModel authenticatorModel) throws IOException {
        return authenticationRepoPort.retrieveAccessToken(authenticatorModel);
    }
}
