package com.satish.blogme.domain.port;

import com.satish.blogme.domain.model.AuthenticatorModel;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import java.io.IOException;

public interface AuthenticationPort {
    OAuth2AccessToken retrieveAccessToken(AuthenticatorModel authenticatorModel) throws IOException;
}
