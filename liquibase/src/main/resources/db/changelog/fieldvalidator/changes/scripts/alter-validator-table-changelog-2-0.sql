ALTER TABLE tbl_validator
ADD CONSTRAINT chk_validator_data_type CHECK (data_type in ('INTEGER', 'DECIMAL', 'BOOLEAN', 'STRING'))