CREATE PROCEDURE `PROC_UPDATE_VALIDATOR_VALUE_OF_FIELD`(
	IN `fieldValidatorId` INT,
	IN `newValue` VARCHAR(255)
)
BEGIN
	SELECT COUNT(*), id INTO @insertedCount, @insertedId
	FROM tbl_validator_value
	WHERE
	value = newValue;

	IF(@insertedCount = 0) THEN
		SELECT validator_value_id INTO @id FROM tbl_field_validator
		WHERE id = fieldValidatorId;

		SELECT COUNT(*) INTO @validatorCount FROM tbl_field_validator
		WHERE validator_value_id = @id;

		IF(@validatorCount > 1) THEN

			INSERT INTO tbl_validator_value(value)
			VALUE (newValue);

			UPDATE tbl_field_validator
		    SET validator_value_id = validatorValueId
		    WHERE id = fieldValidatorId;

		ELSE

			UPDATE tbl_validator_value
			SET value = newValue
			WHERE id = @id;

		END IF;

	ELSE

		UPDATE tbl_field_validator
		SET validator_value_id = @insertedId
		WHERE id = fieldValidatorId;

	END IF;
END

