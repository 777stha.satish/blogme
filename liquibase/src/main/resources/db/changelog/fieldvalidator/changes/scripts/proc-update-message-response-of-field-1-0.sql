CREATE PROCEDURE `PROC_UPDATE_MESSAGE_RESPONSE_OF_FIELD`(
	IN `fieldValidatorId` INT,
	IN `newMessage` VARCHAR(255)
)
BEGIN
	SELECT COUNT(*), id INTO @insertedCount, @insertedId
	FROM tbl_message_response
	WHERE
	message = newMessage;

	IF(@insertedCount = 0) THEN
		SELECT message_response_id INTO @id FROM tbl_field_validator
		WHERE id = fieldValidatorId;

		SELECT COUNT(*) INTO @messageResponseCount FROM tbl_field_validator
		WHERE message_response_id = @id;

		IF(@messageResponseCount > 1) THEN

			SELECT MAX(CODE) INTO @maxCode
			FROM tbl_message_response
			WHERE CODE>=5000;

			IF(@maxCode IS NULL) THEN
				SET @maxCode = 5000;
			ELSE
				SET @maxCode = @maxCode + 1;
			END IF;

			INSERT INTO tbl_message_response(code, message)
			VALUE (@maxCode, newMessage);

			UPDATE tbl_field_validator
			SET message_response_id = LAST_INSERT_ID()
			WHERE id = fieldValidatorId;

		ELSE

			UPDATE tbl_message_response
			SET message = newMessage
			WHERE id = @id;

		END IF;

	ELSE

		UPDATE tbl_field_validator
		SET message_response_id = @insertedId
		WHERE id = fieldValidatorId;

	END IF;
END