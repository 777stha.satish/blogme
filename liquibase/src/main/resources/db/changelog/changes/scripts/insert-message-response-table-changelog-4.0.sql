INSERT INTO tbl_message_response(`code`, `message`)
VALUES
(1000, 'Message response cannot be more than 255 characters'), #Message response max length
(1001, 'Message response cannot be less than 1 character'), #Message response min length
(1002, 'Message response is a required field'),  #Message response required

(1003, 'Field key cannot be more than 255 characters'), #Field key max length
(1004, 'Field key cannot be less than 2 characters'), #Field key min length
(1005, 'Field key is a required field'),  #Field key required

(1006, 'Field label cannot be more than 255 characters'), #Label max length
(1007, 'Field label cannot be less than 2 characters'), #Label min length
(1008, 'Field label is a required field'),  #Label required

(1009, 'Field name cannot be more than 255 characters'), #field name max length
(1010, 'Field name cannot be less than 2 characters'), #field name min length
(1011, 'Field name is a required field'),  #field name required

(1012, 'Placeholder cannot be more than 255 characters'), #Placeholder name max length
(1013, 'Placeholder cannot be less than 5 characters'), #Placeholder name min length

(1014, 'Validator value cannot be more than 750 characters'), #Validator value max length
(1015, 'Validator value cannot be less than 1 character'), #Validator value min length
(1016, 'Validator value is a required field')  #Validator value required