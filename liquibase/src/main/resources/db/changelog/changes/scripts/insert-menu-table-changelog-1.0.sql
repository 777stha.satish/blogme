INSERT INTO tbl_menu(name, path, enabled, `ORDER`, icon)
VALUES
("Dashboard", "/main/dashboard", 1, 1, "fas fa-home"),
("Field Validator", "/main/field-validator", 1, 2, "fas fa-cogs"),
("Logout", "/auth/login", 1, 500, "fas fa-sign-out-alt")
