INSERT INTO tbl_field(`field_key`, `label`, `name`, `placeholder`, `status`)
VALUES
('fieldKey', 'Field Key', 'fieldKey', 'Enter unique identifier for the field', 1),
('label', 'Label', 'label', 'Enter label for the field', 1),
('name', 'Field name', 'name', 'Enter name of the field', 1),
('placeholder', 'Placeholder', 'placeholder', 'Enter placeholder for the field', 1),
('messageResponse', 'Message Response', 'message', 'Enter message response for the provided validator', 1),
('validatorValue', 'Validator Value', 'value', 'Enter value for the provided validator', 1)