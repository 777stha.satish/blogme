INSERT INTO tbl_field_validator(`field_id`, `message_response_id`, `validator_id`, `validator_value_id`,`status`)
VALUES
(1, 4, 1, 1, 1), #field key max length
(1, 5, 2, 4, 1),  #field key min length
(1, 6, 5, 3, 1),  #field key required

(2, 7, 1, 1, 1), #field label max length
(2, 8, 2, 4, 1),  #field label min length
(2, 9, 5, 3, 1),  #field label required

(3, 10, 1, 1, 1), #field name max length
(3, 11, 2, 4, 1),  #field name min length
(3, 12, 5, 3, 1),  #field name required

(4, 13, 1, 1, 1), #field placeholder max length
(4, 14, 2, 5, 1),  #field placeholder min length

(5, 1, 1, 1, 1), #message response max length
(5, 2, 2, 2, 1),  #message response min length
(5, 3, 5, 3, 1),  #message response required

(6, 15, 1, 6, 1), #validator value max length
(6, 16, 2, 2, 1),  #validator value min length
(6, 17, 5, 3, 1)  #validator value required