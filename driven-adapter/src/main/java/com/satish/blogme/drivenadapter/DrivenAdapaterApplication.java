package com.satish.blogme.drivenadapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackageClasses = {DrivenAdapaterApplication.class})
public class DrivenAdapaterApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrivenAdapaterApplication.class, args);
	}

}
