package com.satish.blogme.drivenadapter.mapper;

public interface SingleMapper<E, M> {

    M toModel(E entity);

    E toEntity(M model);

}
