package com.satish.blogme.drivenadapter.mapper.collection.concrete;

import com.satish.blogme.drivenadapter.mapper.SingleMapper;

import java.util.Collection;
import java.util.stream.Stream;

public abstract class StreamMapper<E, M> {

    private SingleMapper<E, M> singleMapper;

    public StreamMapper(SingleMapper<E, M> singleMapper) {
        this.singleMapper = singleMapper;
    }

    public Stream<M> mapToStreamModel(Collection<E> entities) {
        return entities.stream()
                .map(singleMapper::toModel);
    }

    public Stream<E> mapToStreamEntity(Collection<M> models) {
        return models.stream()
                .map(singleMapper::toEntity);
    }
}
