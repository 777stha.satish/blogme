package com.satish.blogme.drivenadapter.mapper.collection.concrete;

import com.satish.blogme.drivenadapter.mapper.SingleMapper;
import com.satish.blogme.drivenadapter.mapper.collection.CollectionMapper;
import com.satish.blogme.drivenadapter.util.CollectionKey;

public class CollectionMapperFactory {
    private CollectionMapperFactory() {}
    public static <E, M> CollectionMapper<E, M> getMapper(CollectionKey key, SingleMapper<E, M> singleMapper) {
        if (key == null) {
            return null;
        }
        if (key.equals(CollectionKey.SET)) {
            return new SetMapper<>(singleMapper);
        } else if (key.equals(CollectionKey.LIST)) {
            return new ListMapper<>(singleMapper);
        }
        return null;
    }
}
