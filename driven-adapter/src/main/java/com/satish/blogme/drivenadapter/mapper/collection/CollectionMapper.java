package com.satish.blogme.drivenadapter.mapper.collection;


import java.util.Collection;

public interface CollectionMapper<E, M> {
    Collection<E> toEntities(Collection<M> models);
    Collection<M> toModels(Collection<E> entities);
}
