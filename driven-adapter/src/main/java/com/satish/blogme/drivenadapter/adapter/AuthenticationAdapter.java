package com.satish.blogme.drivenadapter.adapter;

import com.satish.blogme.domain.exception.RestTemplateException;
import com.satish.blogme.domain.model.AuthenticatorModel;
import com.satish.blogme.domain.port.driven.AuthenticationRepoPort;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;

@Transactional
@Repository
public class AuthenticationAdapter implements AuthenticationRepoPort {

    @Value("${tokenEndPoint}")
    private String tokenEndPoint;

    @Value("${clientUsername}")
    private String clientUsername;

    @Value("${clientPassword}")
    private String clientPassword;

    @Value("${grantTypeValue}")
    private String grantTypeValue;


    @Override
    public OAuth2AccessToken retrieveAccessToken(AuthenticatorModel authenticatorModel) throws IOException {
        try {
            return new ObjectMapper()
                    .readValue(requestAuthServer(getHeader(), getURI(authenticatorModel)).getBody(), OAuth2AccessToken.class);
        } catch (HttpClientErrorException ex) {
            throw new RestTemplateException(ex.getResponseBodyAsString());
        }
    }

    @Override
    public HttpHeaders getHeader() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBasicAuth(clientUsername, clientPassword);
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        return httpHeaders;
    }

    @Override
    public URI getURI(AuthenticatorModel authenticatorModel) {
        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(tokenEndPoint)
                .queryParam("grant_type", grantTypeValue)
                .queryParam("username", authenticatorModel.getUsername())
                .queryParam("password",  authenticatorModel.getPassword());
        return uriComponentsBuilder.buildAndExpand().toUri();
    }

    @Override
    public ResponseEntity<String> requestAuthServer(HttpHeaders headers, URI uri) {
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);
        return new RestTemplate().exchange(uri, HttpMethod.POST, httpEntity, String.class);
    }
}
