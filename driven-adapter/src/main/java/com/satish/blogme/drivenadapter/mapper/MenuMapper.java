package com.satish.blogme.drivenadapter.mapper;

import com.satish.blogme.domain.model.MenuModel;
import com.satish.blogme.drivenadapter.entity.Menu;

public interface MenuMapper extends SingleMapper<Menu, MenuModel> {
}
