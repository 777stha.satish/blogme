package com.satish.blogme.drivenadapter.mapper.collection.concrete;

import com.satish.blogme.drivenadapter.mapper.SingleMapper;
import com.satish.blogme.drivenadapter.mapper.collection.CollectionMapper;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

public class SetMapper<E, M> extends StreamMapper<E, M> implements CollectionMapper<E, M> {

    public SetMapper(SingleMapper<E, M> singleMapper) {
        super(singleMapper);
    }

    @Override
    public Collection<E> toEntities(Collection<M> models) {
        if (models.isEmpty()) {
            return Collections.emptySet();
        }
        return mapToStreamEntity(models)
                .collect(Collectors.toSet());
    }

    @Override
    public Collection<M> toModels(Collection<E> entities) {
        if (entities.isEmpty()) {
            return Collections.emptySet();
        }
        return mapToStreamModel(entities)
                .collect(Collectors.toSet());
    }
}
