package com.satish.blogme.drivenadapter.mapper.impl;

import com.satish.blogme.drivenadapter.mapper.MenuMapper;
import com.satish.blogme.domain.model.MenuModel;
import com.satish.blogme.drivenadapter.entity.Menu;
import org.springframework.stereotype.Service;

@Service("menuMapperDTOImpl")
public class MenuMapperImpl implements MenuMapper {

    @Override
    public MenuModel toModel(Menu entity) {

        if (entity == null) {
            return null;
        }

        MenuModel menuModel = new MenuModel();
        menuModel.setId(entity.getId());
        menuModel.setName(entity.getName());
        menuModel.setPath(entity.getPath());
        menuModel.setEnabled(entity.isEnabled());
        menuModel.setOrder(entity.getOrder());
        menuModel.setIcon(entity.getIcon());

        return menuModel;
    }

    @Override
    public Menu toEntity(MenuModel model) {

        if (model == null) {
            return null;
        }

        Menu menu = new Menu();
        menu.setId(model.getId());
        menu.setName(model.getName());
        menu.setPath(model.getPath());
        menu.setEnabled(model.isEnabled());
        menu.setOrder(model.getOrder());
        menu.setIcon(model.getIcon());

        return menu;
    }
}
