package com.satish.blogme.drivenadapter.util;

public enum CollectionKey {
    SET,
    LIST
}
