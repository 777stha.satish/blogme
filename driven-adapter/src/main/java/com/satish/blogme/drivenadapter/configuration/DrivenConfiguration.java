package com.satish.blogme.drivenadapter.configuration;

import com.satish.fieldvalidator.secondaryadapter.configuration.FieldValidatorDrivenConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan(basePackages = "com.satish.blogme.drivenadapter")
@EntityScan(basePackages = "com.satish.blogme.drivenadapter.entity")
@EnableJpaRepositories(basePackages = "com.satish.blogme.drivenadapter.repository")
@PropertySource("classpath:driven.properties")
public class DrivenConfiguration extends FieldValidatorDrivenConfiguration {
}
