package com.satish.blogme.drivenadapter.repository;

import com.satish.blogme.drivenadapter.entity.Menu;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MenuRepository extends JpaRepository<Menu, Integer> {
    Optional<List<Menu>> getMenuByEnabledOrderByOrderAsc(boolean enabled);
}
