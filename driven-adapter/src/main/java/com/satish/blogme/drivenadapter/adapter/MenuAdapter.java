package com.satish.blogme.drivenadapter.adapter;

import com.satish.blogme.drivenadapter.mapper.MenuMapper;
import com.satish.blogme.drivenadapter.mapper.collection.CollectionMapper;
import com.satish.blogme.drivenadapter.mapper.collection.concrete.CollectionMapperFactory;
import com.satish.blogme.drivenadapter.repository.MenuRepository;
import com.satish.blogme.domain.model.MenuModel;
import com.satish.blogme.domain.port.driven.MenuRepoPort;
import com.satish.blogme.drivenadapter.entity.Menu;
import com.satish.blogme.drivenadapter.util.CollectionKey;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Transactional
@Repository
public class MenuAdapter implements MenuRepoPort {

    private final MenuRepository menuRepository;

    private final CollectionMapper<Menu, MenuModel> listMapper;

    public MenuAdapter(final MenuRepository menuRepository, final MenuMapper menuMapper) {
        this.menuRepository = menuRepository;
        this.listMapper = CollectionMapperFactory.getMapper(CollectionKey.LIST, menuMapper);
    }

    @Override
    public List<MenuModel> getMenuByEnabled(boolean enabled) {

        List<Menu> menus = new ArrayList<>(this.menuRepository
                .getMenuByEnabledOrderByOrderAsc(enabled)
                .orElse(Collections.emptyList()));

        return (List<MenuModel>)listMapper
                .toModels(menus);
    }
}
